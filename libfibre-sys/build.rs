extern crate bindgen;

use std::env;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::process::Stdio;

fn main() {
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());

    // Build libfibre first
    Command::new("make")
        // Note: we keep inline functions here for bindgen to generate bindings to inline functions
        // This may have some performance implications, so it's better if we can remove this in the future
        // For now, LTO should be able to handle most of the cases
        .env("CXXFLAGS", "-O3 -fkeep-inline-functions -fPIC")
        .env("CFLAGS", "-O3 -fkeep-inline-functions -fPIC")
        .current_dir("../deps/libfibre")
        .args(["all"])
        .stdout(Stdio::inherit())
        .status()
        .unwrap();

    // Trigger rebuild when the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.hpp");
    println!("cargo:rerun-if-changed=../deps/libfibre");

    // Generate bindings
    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .size_t_is_usize(true)
        .generate_inline_functions(true)
        .clang_arg("-I../deps/libfibre/src")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .unwrap();

    // Link with libfibre
    println!("cargo:rustc-link-lib=static=fibre");
    println!("cargo:rustc-flags=-l dylib=stdc++");
    println!(
        "cargo:rustc-link-search=native={}/deps/libfibre/src/",
        Path::new("..").canonicalize().unwrap().to_str().unwrap()
    );
}
