use crate::FutureShim;
use std::pin::Pin;
use std::task::{Context, Poll};

pub trait FakeStream: Unpin {
    type Item;
    fn poll_next(
        self: Pin<&mut Self>, 
        cx: &mut Context<'_>
    ) -> Poll<Option<Self::Item>>;

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, None)
    }

    fn block_next(&mut self) -> Option<Self::Item> {
        loop {
            match Pin::new(&mut *self).poll_next(unsafe { std::mem::transmute(std::ptr::null::<()>()) }) {
                Poll::Pending => continue,
                Poll::Ready(res) => return res
            }
        }
    }
}

impl<T: Iterator + Unpin> FakeStream for T {
    type Item = T::Item;

    fn poll_next(
            self: Pin<&mut Self>, 
            _cx: &mut Context<'_>
        ) -> Poll<Option<Self::Item>> {
        Poll::Ready(self.get_mut().next())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.size_hint()
    }

    fn block_next(&mut self) -> Option<Self::Item> {
        self.next()
    }
}

pub struct StreamCompat<T: FakeStream + Sized>(T);

impl<T: FakeStream + Sized> Iterator for StreamCompat<T> {
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.block_next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl<T: FakeStream + Sized> StreamShim for StreamCompat<T> {
    type Inner = Self;

    fn get_inner(&self) -> &Self::Inner {
        self
    }

    fn get_inner_mut(&mut self) -> &mut Self::Inner {
        self
    }

    fn into_inner(self) -> Self::Inner {
        self
    }
}

pub trait StreamShim: Sized {
    type Inner: Iterator;

    fn get_inner(&self) -> &Self::Inner;
    fn get_inner_mut(&mut self) -> &mut Self::Inner;
    fn into_inner(self) -> Self::Inner;

    fn next(&mut self) -> Next<'_, Self> {
        Next { parent: self }
    }
}

pub struct Next<'a, T: StreamShim> {
    parent: &'a mut T
}

impl<T: StreamShim> FutureShim for Next<'_, T> {
    type Output = Option<<T::Inner as Iterator>::Item>;

    fn _await(self) -> Self::Output {
        self.parent.get_inner_mut().next()
    }
}