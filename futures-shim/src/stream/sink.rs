use crate::FutureShim;
use std::marker::PhantomData;
use std::pin::Pin;
use std::task::{Poll, Context};

// There is no std equivalent to Sink, so we define one ourselves.
pub trait SyncSink<Item> {
    type Error;
    fn ready(&mut self) -> Result<(), Self::Error>;
    fn start_send(&mut self, item: Item) -> Result<(), Self::Error>;
    fn flush(&mut self) -> Result<(), Self::Error>;
    fn close(&mut self) -> Result<(), Self::Error>;
}

pub trait FakeSink<Item>: Unpin {
    type Error;

    fn poll_ready(
        self: Pin<&mut Self>, 
        cx: &mut Context<'_>
    ) -> Poll<Result<(), Self::Error>>;
    fn start_send(self: Pin<&mut Self>, item: Item) -> Result<(), Self::Error>;
    fn poll_flush(
        self: Pin<&mut Self>, 
        cx: &mut Context<'_>
    ) -> Poll<Result<(), Self::Error>>;
    fn poll_close(
        self: Pin<&mut Self>, 
        cx: &mut Context<'_>
    ) -> Poll<Result<(), Self::Error>>;

    fn block_ready(&mut self) -> Result<(), Self::Error> {
        loop {
            match Pin::new(&mut *self).poll_ready(unsafe { std::mem::transmute(std::ptr::null::<()>()) }) {
                Poll::Pending => continue,
                Poll::Ready(res) => return res
            }
        }
    }

    fn block_flush(&mut self) -> Result<(), Self::Error> {
        loop {
            match Pin::new(&mut *self).poll_flush(unsafe { std::mem::transmute(std::ptr::null::<()>()) }) {
                Poll::Pending => continue,
                Poll::Ready(res) => return res
            }
        }
    }

    fn block_close(&mut self) -> Result<(), Self::Error> {
        loop {
            match Pin::new(&mut *self).poll_close(unsafe { std::mem::transmute(std::ptr::null::<()>()) }) {
                Poll::Pending => continue,
                Poll::Ready(res) => return res
            }
        }
    }
}

impl<I, T> FakeSink<I> for T where T: SyncSink<I> + Unpin {
    type Error = T::Error;

    fn start_send(self: Pin<&mut Self>, item: I) -> Result<(), Self::Error> {
        self.get_mut().start_send(item)
    }

    fn poll_ready(
            self: Pin<&mut Self>, 
            _cx: &mut Context<'_>
        ) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(self.get_mut().ready())
    }

    fn poll_close(
            self: Pin<&mut Self>, 
            _cx: &mut Context<'_>
        ) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(self.get_mut().close())
    }

    fn poll_flush(
            self: Pin<&mut Self>, 
            _cx: &mut Context<'_>
        ) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(self.get_mut().flush())
    }

    fn block_ready(&mut self) -> Result<(), Self::Error> {
        self.ready()
    }

    fn block_flush(&mut self) -> Result<(), Self::Error> {
        self.flush()
    }

    fn block_close(&mut self) -> Result<(), Self::Error> {
        self.close()
    }
}

pub struct SinkCompat<I, T: FakeSink<I> + Sized>(T, PhantomData<I>);

impl<I, T: FakeSink<I> + Sized> SyncSink<I> for SinkCompat<I, T> {
    type Error = T::Error;

    fn ready(&mut self) -> Result<(), Self::Error> {
        self.0.block_ready()
    }

    fn start_send(&mut self, item: I) -> Result<(), Self::Error> {
        Pin::new(&mut self.0).start_send(item)
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        self.0.block_flush()
    }

    fn close(&mut self) -> Result<(), Self::Error> {
        self.0.block_close()
    }
}

impl<I, T: FakeSink<I> + Sized> SinkShim<I> for SinkCompat<I, T> {
    type Inner = Self;

    fn get_inner(&self) -> &Self::Inner {
        self
    }

    fn get_inner_mut(&mut self) -> &mut Self::Inner {
        self
    }

    fn into_inner(self) -> Self::Inner {
        self
    }
}

pub trait SinkShim<Item>: Sized {
    type Inner: SyncSink<Item>;

    fn get_inner(&self) -> &Self::Inner;
    fn get_inner_mut(&mut self) -> &mut Self::Inner;
    fn into_inner(self) -> Self::Inner;

    fn send(&mut self, item: Item) -> Send<'_, Self, Item> {
        Send { parent: self, item }
    }
}

pub struct Send<'a, T: SinkShim<I>, I> {
    parent: &'a mut T,
    item: I
}

impl<'a, T: SinkShim<I>, I> FutureShim for Send<'a, T, I> {
    type Output = Result<(), <T::Inner as SyncSink<I>>::Error>;

    fn _await(self) -> Self::Output {
        self.parent.get_inner_mut().start_send(self.item)?;
        self.parent.get_inner_mut().flush()
    }
}