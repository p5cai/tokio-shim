mod frames;
mod framed;

pub use framed::*;

use std::io::{Error, ErrorKind, Read, BorrowedBuf, Write, IoSlice, self};
use std::mem::MaybeUninit;
use bytes::{Buf, BufMut, BytesMut};

pub(crate) fn read_buf<T: Read, B: BufMut>(
    io: &mut T,
    buf: &mut B
) -> io::Result<usize> {
    if !buf.has_remaining_mut() {
        return Ok(0);
    }

    let n = {
        let dst = buf.chunk_mut();

        let dst = unsafe { &mut *(dst as *mut _ as *mut [MaybeUninit<u8>]) };
        let mut buf = BorrowedBuf::from(dst);
        let ptr = buf.filled().as_ptr();

        io.read_buf(buf.unfilled())?;

        assert_eq!(ptr, buf.filled().as_ptr());

        buf.filled().len()
    };

    unsafe {
        buf.advance_mut(n);
    }

    Ok(n)
}

pub(crate) fn write_buf<T: Write, B: Buf>(
    io: &mut T,
    buf: &mut B
) -> io::Result<usize> {
    const MAX_BUFS: usize = 64;

    if !buf.has_remaining() {
        return Ok(0);
    }

    let n = if io.is_write_vectored() {
        let mut slices = [IoSlice::new(&[]); MAX_BUFS];
        let cnt = buf.chunks_vectored(&mut slices);
        io.write_vectored(&slices[..cnt])?
    } else {
        io.write(buf.chunk())?
    };

    buf.advance(n);

    Ok(n)
}

pub trait Encoder<Item> {
    type Error: From<Error>;

    fn encode(
        &mut self, 
        item: Item, 
        dst: &mut BytesMut
    ) -> Result<(), Self::Error>;
}

pub trait Decoder {
    type Item;
    type Error: From<Error>;

    fn decode(
        &mut self, 
        src: &mut BytesMut
    ) -> Result<Option<Self::Item>, Self::Error>;

    fn decode_eof(
        &mut self,
        buf: &mut BytesMut
    ) -> Result<Option<Self::Item>, Self::Error> {
        match self.decode(buf)? {
            Some(frame) => Ok(Some(frame)),
            None => {
                if buf.is_empty() {
                    Ok(None)
                } else {
                    Err(Error::new(ErrorKind::Other, "bytes remaining on stream").into())
                }
            }
        }
    }

    // TODO: The rest (which depends on Framed)
}