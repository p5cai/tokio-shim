use std::borrow::{Borrow, BorrowMut};
use std::io::{Read, Write};
use crate::codec::Decoder;
use crate::codec::frames::{ReadFrame, WriteFrame, RWFrames, self};
use crate::stream::{StreamShim, SinkShim, SyncSink};

use super::Encoder;

pub struct Framed<T, U> {
    inner: T,
    codec: U,
    state: RWFrames
}

impl<T, U> Framed<T, U> where T: Read + Write {
    pub fn new(inner: T, codec: U) -> Framed<T, U> {
        Framed { inner, codec, state: RWFrames::default() }
    }
}

impl<T, U> StreamShim for Framed<T, U> where T: Read, U: Decoder {
    type Inner = Self;

    fn get_inner(&self) -> &Self::Inner {
        self
    }

    fn get_inner_mut(&mut self) -> &mut Self::Inner {
        self
    }

    fn into_inner(self) -> Self::Inner {
        self
    }
}

impl<T, I, U> SinkShim<I> for Framed<T, U> where T: Write, U: Encoder<I> {
    type Inner = Self;

    fn get_inner(&self) -> &Self::Inner {
        self
    }

    fn get_inner_mut(&mut self) -> &mut Self::Inner {
        self
    }

    fn into_inner(self) -> Self::Inner {
        self
    }
}

impl<T, U> Iterator for Framed<T, U> where T: Read, U: Decoder {
    type Item = Result<U::Item, U::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let state: &mut ReadFrame = self.state.borrow_mut();

        // Here we implement the same state machine as in the original FramedImpl in Tokio
        // except we always block while reading a new frame until something can be returned,
        // instead of returning Poll::Pending
        loop {
            if state.has_errored {
                // Reset, as we have already returned the error
                state.is_readable = false;
                state.has_errored = false;
                // This signifies that the Iterator has finished, but
                // it is valid to start iterating again in the future (as per Iterator documentation)
                return None;
            }

            if state.is_readable {
                if state.eof {
                    // TODO: Implement the traits to enable the use of ? here.
                    let frame = match self.codec.decode_eof(&mut state.buffer) {
                        Err(err) => {
                            state.has_errored = true;
                            return Some(Err(err));
                        },
                        Ok(res) => res
                    };
                    if frame.is_none() {
                        state.is_readable = false; // prepare pausing -> paused
                    }
                    return frame.map(Ok);
                }

                match self.codec.decode(&mut state.buffer) {
                    Err(err) => {
                        state.has_errored = true;
                        return Some(Err(err));
                    },
                    Ok(res) => {
                        if let Some(frame) = res {
                            return Some(Ok(frame));
                        }
                    }
                }

                state.is_readable = false;
            }

            state.buffer.reserve(1);

            let bytect = match crate::codec::read_buf(&mut self.inner, &mut state.buffer) {
                Err(err) => {
                    state.has_errored = true;
                    return Some(Err(err.into()));
                },
                Ok(bytect) => bytect
            };

            if bytect == 0 {
                if state.eof {
                    // Finished -- already eof
                    return None;
                }

                state.eof = true;
            } else {
                state.eof = false;
            }

            state.is_readable = true;
        }
    }
}

impl<T, I, U> SyncSink<I> for Framed<T, U> where T: Write, U: Encoder<I> {
    type Error = U::Error;

    fn ready(&mut self) -> Result<(), Self::Error> {
        let state: &WriteFrame = self.state.borrow();
        if state.buffer.len() > frames::BACKPRESSURE_BOUNDARY {
            self.inner.flush().map_err(|e| e.into())
        } else {
            Ok(())
        }
    }

    fn start_send(&mut self, item: I) -> Result<(), Self::Error> {
        let state: &mut WriteFrame = self.state.borrow_mut();
        self.codec.encode(item, &mut state.buffer)
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        let state: &mut WriteFrame = self.state.borrow_mut();

        while !state.buffer.is_empty() {
            let n = crate::codec::write_buf(&mut self.inner, &mut state.buffer)?;

            if n == 0 {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::WriteZero,
                    "failed to \
                     write frame to transport",
                )
                .into());
            }
        }

        self.inner.flush()?;
        Ok(())
    }

    fn close(&mut self) -> Result<(), Self::Error> {
        self.flush()?;
        // TODO: Actually drop and close the inner
        Ok(())
    }
}