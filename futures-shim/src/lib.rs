// These features are not stable yet due to the lack of
// higher-ranked trait bounds support in general. However,
// we won't run into the issue because our Fn traits never
// have actual input parameters.
#![feature(unboxed_closures)]
#![feature(fn_traits)]
#![feature(const_pin)]
#![feature(read_buf)]
#![feature(can_vector)]

pub mod io;
pub mod stream;
pub mod codec;

use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};

pub trait FutureShim {
    type Output;
    fn _await(self) -> Self::Output;
}

pub struct FutureShimClosure<T: FnOnce<()>>(T);

impl<T> FutureShim for FutureShimClosure<T> where T: FnOnce<()> {
    type Output = T::Output;

    fn _await(self) -> Self::Output {
        self.0.call_once(())
    }
}

// This trait asserts that the Future is actually a blocking "fake" Future,
// and will never make use of the context parameter of the poll function.
// When it is invoked via FutureShimCompat, the context will be set to a nullptr
pub unsafe trait FakeFuture: Sized + Future + Unpin {
    // This function exists so that specific implementations can override the default behavior
    // of using the poll() method to implement _await().
    // This is used in FutureShimCompat to avoid calling all the way
    // into Future::poll().
    fn block_for_result(mut self) -> Self::Output {
        loop {
            let res = Future::poll(Pin::new(&mut self), unsafe { std::mem::transmute(std::ptr::null::<()>()) });
            if let Poll::Ready(res) = res {
                return res;
            }
        }
    }
}

// A wrapper to make our custom future shim types compatible with the original Futures as well
// so that we can deal with custom Future impls. A pass-through impl for FutureShim is
// also provided by implementing the `block_for_result` method specifically for this type
// TODO: We probably don't need the Option<> inside, as poll() is not even supposed to be
// called multiple times. But for now we need it because there is no way to guarantee this
// fact.
pub struct FutureShimCompat<T: FutureShim + Unpin>(Option<T>);

unsafe impl<T: FutureShim + Unpin> FakeFuture for FutureShimCompat<T> {
    fn block_for_result(mut self) -> Self::Output {
        self.0.take().unwrap()._await()
    }
}

impl<T> FutureShimCompat<FutureShimClosure<T>> where T: FnOnce<()> + Unpin {
    pub fn from_closure(f: T) -> FutureShimCompat<FutureShimClosure<T>> {
        FutureShimCompat(Some(FutureShimClosure(f)))
    }
}

impl<T> Future for FutureShimCompat<T> where T: FutureShim + Unpin {
    type Output = T::Output;

    fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
        return Poll::Ready(Pin::into_inner(self).0.take().unwrap()._await())
    }
}

// Implementation of the FutureShim trait for all "fake" futures that
// ultimately poll on a FutureShim object somewhere down the line.
// By default, the _await() method here calls poll() on the underlying
// future until it returns Poll::Ready(), and all poll calls are expected
// to be actually blocking (ultimately block on an I/O call in the FutureShim).
// This is necessary because one outer future (either generated or manually
// implemented) may only become ready when multiple underlying futures in a chain
// becomes ready.
// Note: The blocking behavior works fine for simple Future transformations that
// do not involve executing multiple underlying futures simultaneously. For those
// cases, manual intervention is needed to ensure parallelization. We could
// unconditionally apply parallelization (e.g. spawning a new thread and wait
// on a semaphore), but that would be too much overhead for the simple cases where
// only a transformation is needed.
// If this is a FutureShimCompat object itself, then the polling loop
// will always finish in exactly 1 iteration (optimized away by the specific
// `block_for_result` implementation on FutureShimCompat)
impl<T: FakeFuture> FutureShim for T {
    type Output = T::Output;

    fn _await(self) -> Self::Output {
        self.block_for_result()
    }
}
