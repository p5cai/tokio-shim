use crate::FutureShim;

use std::io::{Result, Write};

pub trait AsyncWriteShim: Sized {
    type Inner: Write + Sized;

    fn get_inner(&self) -> &Self::Inner;
    fn get_inner_mut(&mut self) -> &mut Self::Inner;
    fn into_inner(self) -> Self::Inner;

    fn write<'a>(&'a mut self, buf: &'a [u8]) -> WriteShim<'a, Self> {
        WriteShim { parent: self, buf }
    }

    fn write_all<'a>(&'a mut self, buf: &'a [u8]) -> WriteAllShim<'a, Self> {
        WriteAllShim { parent: self, buf }
    }
}

pub struct WriteShim<'a, T: AsyncWriteShim> {
    parent: &'a mut T,
    buf: &'a [u8]
}

impl<'a, T: AsyncWriteShim> FutureShim for WriteShim<'a, T> {
    type Output = Result<usize>;

    fn _await(self) -> Self::Output {
        self.parent.get_inner_mut().write(self.buf)
    }
}

pub struct WriteAllShim<'a, T: AsyncWriteShim> {
    parent: &'a mut T,
    buf: &'a [u8]
}

impl<'a, T: AsyncWriteShim> FutureShim for WriteAllShim<'a, T> {
    type Output = Result<()>;

    fn _await(self) -> Self::Output {
        self.parent.get_inner_mut().write_all(self.buf)
    }
}

