use std::io::BufRead;

pub trait AsyncBufReadShim: Sized {
    type Inner: BufRead + Sized;

    fn get_inner(&self) -> &Self::Inner;
    fn get_inner_mut(&mut self) -> &mut Self::Inner;
    fn into_inner(self) -> Self::Inner;

    fn read_until<'a>(
        &'a mut self, 
        byte: u8, 
        buf: &'a mut Vec<u8>
    ) -> ReadUntilShim<'a, Self> {
        ReadUntilShim { parent: self, byte, buf }
    }

    fn read_line<'a>(&'a mut self, buf: &'a mut String) -> ReadLineShim<'a, Self> {
        ReadLineShim { parent:self, buf }
    }

    fn split(self, byte: u8) -> SplitShim<Self> {
        SplitShim { parent: self, byte }
    }

    fn fill_buf(&mut self) -> FillBufShim<'_, Self> {
        FillBufShim { parent: self }
    }

    fn consume(&mut self, amt: usize) {
        self.get_inner_mut().consume(amt)
    }

    fn lines(self) -> LinesShim<Self> {
        LinesShim { parent: self }
    }
}

pub struct ReadUntilShim<'a, T: AsyncBufReadShim> {
    parent: &'a mut T,
    byte: u8,
    buf: &'a mut Vec<u8>
}

pub struct ReadLineShim<'a, T: AsyncBufReadShim> {
    parent: &'a mut T,
    buf: &'a mut String
}

pub struct SplitShim<T: AsyncBufReadShim> {
    parent: T,
    byte: u8
}

pub struct FillBufShim<'a, T: AsyncBufReadShim> {
    parent: &'a mut T
}

pub struct LinesShim<T: AsyncBufReadShim> {
    parent: T
}