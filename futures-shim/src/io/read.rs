use crate::FutureShim;

use std::io as sio;
use std::pin::Pin;
use std::task::{Context, Poll};
use sio::{Read, Result, BorrowedBuf};

// Similar to FakeFuture, this trait (and the following wrapper structs) enable us to handle
// the manually-implemented async read and write objects
// This does not extend Read, because we want to be able to port pure AsyncRead implementations over to ours
// The Read implementation will be provided by the AsyncReadCompat struct.
// This is safe, unlike FakeFuture, because there is no standalone AsyncRead trait to worry about
pub trait FakeAsyncRead: Unpin {
    fn poll_read(
        self: Pin<&mut Self>, 
        cx: &mut Context<'_>, 
        buf: &mut BorrowedBuf<'_>
    ) -> Poll<Result<()>>;

    fn block_read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let mut rbuf = BorrowedBuf::from(buf);
        loop {
            match Pin::new(&mut *self).poll_read(unsafe { std::mem::transmute(std::ptr::null::<()>()) }, &mut rbuf) {
                Poll::Pending => continue,
                Poll::Ready(Ok(())) => return Ok(rbuf.len()),
                Poll::Ready(Err(e)) => return Err(e)
            }
        }
    }
}

impl<T: Read + Unpin> FakeAsyncRead for T {
    fn poll_read(
            self: Pin<&mut Self>, 
            _cx: &mut Context<'_>, 
            buf: &mut BorrowedBuf<'_>
        ) -> Poll<Result<()>> {
        Poll::Ready(self.get_mut().read_buf(buf.unfilled()))
    }

    fn block_read(&mut self, buf: &mut [u8]) -> Result<usize> {
        self.read(buf)
    }
}

// Compat struct because we cannot implement Read for all FakeAsyncRead due to Rust restrictions
pub struct AsyncReadCompat<T: FakeAsyncRead + Sized>(T);

impl<T: FakeAsyncRead + Sized> Read for AsyncReadCompat<T> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        self.0.block_read(buf)
    }
}

impl<T: FakeAsyncRead + Sized> AsyncReadShim for AsyncReadCompat<T> {
    type Inner = Self;

    fn get_inner(&self) -> &Self::Inner {
        self
    }

    fn get_inner_mut(&mut self) -> &mut Self::Inner {
        self
    }

    fn into_inner(self) -> Self::Inner {
        self
    }
}

// Corresponds to AsyncReadExt; not implemented as an extension of FakeAsyncRead
// because we want to be able to access the underlying Read methods directly.
// Not defined as extending Read due to method signature conflicts (which requires
// explicit conversion every single time when called).
// A FakeAsyncRead object gains access to all of these by being wrapped in AsyncReadCompat
pub trait AsyncReadShim: Sized {
    type Inner: Read + Sized;

    fn get_inner(&self) -> &Self::Inner;
    fn get_inner_mut(&mut self) -> &mut Self::Inner;
    fn into_inner(self) -> Self::Inner;

    fn read<'a>(&'a mut self, buf: &'a mut [u8]) -> ReadShim<'a, Self> {
        ReadShim { parent: self, buf }
    }

    // TODO: Finish all the other less-used method impl
}

pub struct ReadShim<'a, T: AsyncReadShim> {
    parent: &'a mut T,
    buf: &'a mut [u8]
}

impl<'a, T: AsyncReadShim> FutureShim for ReadShim<'a, T> {
    type Output = Result<usize>;

    fn _await(self) -> Self::Output {
        self.parent.get_inner_mut().read(self.buf)
    }
}