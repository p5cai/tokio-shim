use crate::handle::{Fibre, JoinHandle, JoinInner, Packet};
use crate::{FibreSafe, Result};
use libfibre_sys as libfibre;
use std::cell::UnsafeCell;
use std::ffi::{c_void, CString};
use std::mem::MaybeUninit;
use std::sync::Arc;

pub struct FibreAttrs {
    native: libfibre::cfibre_attr_t,
}

// TODO: finish all the bindings here
impl FibreAttrs {
    pub fn new() -> FibreAttrs {
        unsafe {
            let mut native: MaybeUninit<libfibre::cfibre_attr_t> = MaybeUninit::uninit();
            if libfibre::cfibre_attr_init(native.as_mut_ptr()) < 0 {
                // TODO: do we need to panic here?
                panic!("Could not initialize fibre attributes");
            }

            FibreAttrs {
                native: native.assume_init(),
            }
        }
    }

    pub fn get_stack_size(&self) -> usize {
        let mut ret = 0;

        unsafe {
            if libfibre::cfibre_attr_getstacksize(&self.native, &mut ret) < 0 {
                ret = 0;
            }
        }

        return ret;
    }

    pub fn set_stack_size(&mut self, size: usize) {
        unsafe {
            libfibre::cfibre_attr_setstacksize(&mut self.native, size);
        }
    }
}

impl Drop for FibreAttrs {
    fn drop(&mut self) {
        unsafe {
            libfibre::cfibre_attr_destroy(&mut self.native);
        }
    }
}

unsafe extern "C" fn _fibre_main(_real_main: *mut c_void) -> *mut c_void {
    // TODO: stack overflow handler
    let f = Box::from_raw(_real_main as *mut Box<dyn FnOnce()>);
    f();
    std::ptr::null_mut()
}

/*
 * Builder for a new Fibre with custom attributes and chain calling support
 * This must be consumed by the spawn() method
 * Mirrors the Builder struct of a regular thread
 */
pub struct Builder {
    name: Option<String>,
    attrs: FibreAttrs,
}

impl Builder {
    pub fn new() -> Builder {
        Builder {
            name: None,
            attrs: FibreAttrs::new(),
        }
    }

    pub fn name(mut self, name: String) -> Self {
        self.name = Some(name);
        self
    }

    pub fn stack_size(mut self, size: usize) -> Self {
        self.attrs.set_stack_size(size);
        self
    }

    /*
     * Spawns a new fibre with a closure as its main function
     * Note that this function is only safe to use with a function that is
     * marked FibreSafe. See the description of FibreSafe for details on
     * the safety guarantees needed.
     */
    pub fn spawn<F, T>(self, f: F) -> std::io::Result<JoinHandle<T>>
    where
        F: FibreSafe + FnOnce() -> T,
        F: Send + 'static,
        T: Send + 'static,
    {
        unsafe { self.spawn_unchecked(f) }
    }

    /*
     * Unsafe implementation of spawn()
     * It is unsafe because the lifetime 'a is not bounded, which means
     * that the thread may outlive the closure.
     * In addition, the FibreSafe trait bound is not enforced.
     * Callers must ensure that the thread is destroyed before 'a becomes invalid
     */
    pub unsafe fn spawn_unchecked<'a, F, T>(self, f: F) -> std::io::Result<JoinHandle<T>>
    where
        F: FnOnce() -> T,
        F: Send + 'a,
        T: Send + 'a,
    {
        if !crate::INITIALIZED {
            // TODO: what if the process was forked without re-intializing runtime?
            // This would leave INITIALIZED to be true, but the runtime is not actually initialized
            panic!("libfibre runtime uninitialized");
        }

        let cname = self.name.map(|name| {
            CString::new(name).expect("thread name may not contain interior null bytes")
        });

        let my_fibre = Fibre::new(cname);
        let their_fibre = my_fibre.clone();

        // UnsafeCell used to pass the return value of the thread
        let my_packet: Arc<UnsafeCell<Option<Result<T>>>> = Arc::new(UnsafeCell::new(None));
        let their_packet = my_packet.clone();

        // main function wrapper for the thread
        let main = move || {
            {
                let parker = &their_fibre.inner.parker;
                unsafe { parker.set_handle_if_necessary(libfibre::cfibre_self()); }
            }

            // Set fibre info
            crate::FIBRE_INFO.with(move |info| {
                info.replace(Some(their_fibre));
            });

            let try_result = std::panic::catch_unwind(std::panic::AssertUnwindSafe(|| f()));

            *their_packet.get() = Some(try_result);
        };

        let mut fibre: MaybeUninit<libfibre::cfibre_t> = MaybeUninit::uninit();

        // We have to box the closure twice, because we cannot cast a fat pointer to a C pointer
        // the fat pointer here comes from `dyn FnOnce()`.
        let boxed_main = Box::new(Box::new(main) as Box<dyn FnOnce()>);

        let ret = libfibre::cfibre_create(
            fibre.as_mut_ptr(),
            &self.attrs.native,
            Some(_fibre_main),
            Box::into_raw(boxed_main) as *mut _,
        );

        if ret < 0 {
            return Err(std::io::Error::from_raw_os_error(ret));
        }

        Ok(JoinHandle {
            inner: JoinInner {
                native: Some(fibre.assume_init()),
                packet: Packet(my_packet),
                fibre: my_fibre,
            },
        })
    }
}
