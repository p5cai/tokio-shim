use libfibre_sys as libfibre;
use std::ffi::c_void;
use std::marker::PhantomData;
use std::sync::atomic::{AtomicUsize, Ordering};

struct LocalKeyValue<T: Send> {
    value: Box<T>,
    key: usize,
}

unsafe extern "C" fn __local_key_destructor<T: 'static + Send>(ptr: *mut c_void) {
    if ptr as usize == 1 {
        return;
    }

    let b = Box::from_raw(ptr as *mut LocalKeyValue<T>);
    // TODO: This does not work in libfibre, because destructors are not run *inside*
    // the fibre, unlike in pthread where destructors are run in the threads
    // This might not actually be an issue though, since if the above is true, then
    // it is not possible to have a reference to the fibre-local values when the destructor
    // is running, even if the Drop implementations try to access fibre-local storage
    // Maybe we can even remove all the Option<> in this case.
    //libfibre::cfibre_setspecific(b.key, 1usize as *mut c_void);
    std::mem::drop(b);
}

pub struct LocalKeyInner<T: 'static + Send> {
    _marker: PhantomData<T>,
    key: AtomicUsize,
}

// LocalKeyInner does not itself contain a reference to T, so
// it is safe to force the Sync trait on it
unsafe impl<T: 'static + Send> Sync for LocalKeyInner<T> {}

impl<T: 'static + Send> LocalKeyInner<T> {
    pub const fn new() -> LocalKeyInner<T> {
        LocalKeyInner {
            _marker: PhantomData,
            key: AtomicUsize::new(0),
        }
    }

    fn lazy_init_native_key(&self) {
        unsafe {
            let mut key: libfibre::cfibre_key_t = 0;
            if libfibre::cfibre_key_create(&mut key, Some(__local_key_destructor::<T>)) < 0 {
                panic!("Could not allocate fibre-local key");
            }

            if key == 0 {
                let mut key1: libfibre::cfibre_key_t = 0;
                if libfibre::cfibre_key_create(&mut key1, Some(__local_key_destructor::<T>)) < 0 {
                    panic!("Could not allocate fibre-local key");
                }
                libfibre::cfibre_key_delete(key);
                key = key1;
            }

            assert_ne!(key, 0);
            
            if let Err(_) =
                self.key
                    .compare_exchange(0, key as usize, Ordering::SeqCst, Ordering::SeqCst)
            {
                // Someone else allocated the key before us
                libfibre::cfibre_key_delete(key);
            }
        }
    }

    fn get_native_key(&self) -> libfibre::cfibre_key_t {
        match self.key.load(Ordering::Relaxed) {
            0 => {
                self.lazy_init_native_key();
                self.get_native_key()
            }
            n => n as libfibre::cfibre_key_t,
        }
    }

    // This function is *unsafe* because the returned lifetime, 'static, is invalid since
    // the T value cannot outlive the thread due to the destructor
    pub unsafe fn get(&self, init: fn() -> T) -> Option<&'static T> {
        let native = self.get_native_key();
        let mut data_ptr = libfibre::cfibre_getspecific(native);

        if data_ptr as usize == 1 {
            // We use 1 as destruction marker
            return None;
        }

        if data_ptr.is_null() {
            // Good ol' double-boxing trick to get rid of fat pointers
            let init_val = Box::new(LocalKeyValue {
                value: Box::new(init()),
                key: native as usize,
            });
            let init_ptr = Box::into_raw(init_val);
            libfibre::cfibre_setspecific(native, std::mem::transmute(init_ptr));
            data_ptr = init_ptr as *mut c_void;
        }

        return Some(
            &std::mem::transmute::<*mut c_void, &'static LocalKeyValue<T>>(data_ptr).value,
        );
    }
}

/*
 * Error type representing access to an already-destroyed fibre-local storage key
 */
pub struct AccessError;

impl std::fmt::Debug for AccessError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("AccessError").finish()
    }
}

impl std::fmt::Display for AccessError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt("already destroyed", f)
    }
}

impl std::error::Error for AccessError {}

/*
 * Mirrors LocalKey for TLS in standard library
 * The main difference here is that the value T must be Send, because fibres
 * can be scheduled onto different OS threads. Technically we need this bound
 * for *all* stack values in a fibre that may cross a yield point boundary, but
 * for now we can only enforce this for LocalKey.
 */
pub struct LocalKey<T: 'static + Send> {
    // accessor must be unsafe since the returned lifetime 'static is invalid
    // all access to thread-local values *must* be scoped using closures
    accessor: unsafe fn() -> Option<&'static T>,
}

impl<T: 'static + Send> LocalKey<T> {
    pub const unsafe fn new(accessor: unsafe fn() -> Option<&'static T>) -> LocalKey<T> {
        LocalKey { accessor }
    }

    #[inline]
    pub fn with<F, R>(&'static self, f: F) -> R
    where
        F: FnOnce(&T) -> R,
    {
        self.try_with(f).expect("Fibre-local key access error")
    }

    #[inline]
    pub fn try_with<F, R>(&'static self, f: F) -> Result<R, AccessError>
    where
        F: FnOnce(&T) -> R,
    {
        unsafe {
            let data = (self.accessor)().ok_or(AccessError)?;
            Ok(f(data))
        }
    }
}

#[macro_export]
macro_rules! fibre_local {
    // empty (base case for the recursion)
    () => {};

    ($(#[$attr:meta])* $vis:vis static $name:ident: $t:ty = const { $init:expr }; $($rest:tt)*) => (
        $crate::__fibre_local_accessor!($(#[$attr])* $vis $name, $t, const $init);
        $crate::fibre_local!($($rest)*);
    );

    ($(#[$attr:meta])* $vis:vis static $name:ident: $t:ty = const { $init:expr }) => (
        $crate::__fibre_local_accessor!($(#[$attr])* $vis $name, $t, const $init);
    );

    // process multiple declarations
    ($(#[$attr:meta])* $vis:vis static $name:ident: $t:ty = $init:expr; $($rest:tt)*) => (
        $crate::__fibre_local_accessor!($(#[$attr])* $vis $name, $t, $init);
        $crate::fibre_local!($($rest)*);
    );

    // handle a single declaration
    ($(#[$attr:meta])* $vis:vis static $name:ident: $t:ty = $init:expr) => (
        $crate::__fibre_local_accessor!($(#[$attr])* $vis $name, $t, $init);
    );
}

#[macro_export]
macro_rules! __fibre_local_accessor {
    (@key $t:ty, const $init:expr) => {{
        #[inline]
        unsafe fn __getit() -> Option<&'static $t> {
            const INIT_EXPR: $t = $init;

            #[inline]
            const fn __init() -> $t { INIT_EXPR }
            static __KEY: $crate::LocalKeyInner<$t> = $crate::LocalKeyInner::new();
            __KEY.get(__init)
        }
    }};
    (@key $t:ty, $init:expr) => {{
        #[inline]
        unsafe fn __getit() -> Option<&'static $t> {
            #[inline]
            const fn __init() -> $t { $init }
            static __KEY: $crate::LocalKeyInner<$t> = $crate::LocalKeyInner::new();
            __KEY.get(__init)
        }

        unsafe {
            $crate::LocalKey::new(__getit)
        }
    }};
    ($(#[$attr:meta])* $vis:vis $name:ident, $t:ty, $($init:tt)*) => {
        $(#[$attr])* $vis const $name: $crate::LocalKey<$t> =
            $crate::__fibre_local_accessor!(@key $t, $($init)*);
    }
}
