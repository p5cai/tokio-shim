/*
 * This module mirrors the JoinHandle implementation in the standard library
 * This is a real Fibre / Thread handle, not the Fibre / Thread object
 */
use crate::Result;

use libfibre_sys as libfibre;
use std::cell::UnsafeCell;
use std::ffi::{CStr, CString};
use std::sync::Arc;

// same as in std::thread, used for passing results of fibres
pub(crate) struct Packet<T>(pub(crate) Arc<UnsafeCell<Option<Result<T>>>>);

unsafe impl<T: Send> Send for Packet<T> {}
unsafe impl<T: Sync> Sync for Packet<T> {}

pub(crate) struct JoinInner<T> {
    // raw libfibre fibre id
    pub(crate) native: Option<libfibre::cfibre_t>,
    pub(crate) fibre: Fibre,
    pub(crate) packet: Packet<T>,
}

impl<T> JoinInner<T> {
    fn join(&mut self) -> Result<T> {
        unsafe {
            // TODO: do we need the retval parameter from cfibre_join?
            // The thread models of Rust and C are quite different, so not sure what purpose retval would serve here
            if libfibre::cfibre_join(self.native.take().unwrap(), std::ptr::null_mut()) < 0 {
                // TODO: better error message
                Err(Box::new("Failed to join the fibre"))
            } else {
                (*self.packet.0.get()).take().unwrap()
            }
        }
    }
}

impl<T> Drop for JoinInner<T> {
    fn drop(&mut self) {
        if self.native.is_some() {
            // The fibre is detached if the handle is dropped
            unsafe {
                libfibre::cfibre_detach(self.native.take().unwrap());
            }
        }
    }
}

pub struct JoinHandle<T> {
    pub(crate) inner: JoinInner<T>,
}

impl<T> JoinHandle<T> {
    pub fn join(mut self) -> Result<T> {
        self.inner.join()
    }

    pub fn fibre(&self) -> &Fibre {
        &self.inner.fibre
    }
}

// An internal handle for parking / unparking the fibre
// Note that this exists only to mirror the official std implementation
// Because Fibres have primitives for parking and unparking,
// we do not need Futex / Mutex magic like what std does.
pub(crate) struct Parker {
    // Use an UnsafeCell to hold the native fibre handle
    // This is fine because this member goes into read-only mode
    // once the field is initialized; it will never be mutated twice
    // and before said (atomic) mutation (initializing native handle),
    // all calls to public APIs will panic.
    native: UnsafeCell<Option<libfibre::cfibre_t>>,
}

// This is fine for the reason stated above: this will only be mutated once
unsafe impl Sync for Parker {}

impl Parker {
    // Create an empty parker; it is only useful when the "native" handle is initialized
    // This is normally done when spawning the fibre
    fn new() -> Parker {
        Parker {
            native: UnsafeCell::new(None),
        }
    }

    pub(crate) unsafe fn set_handle_if_necessary(&self, native: libfibre::cfibre_t) {
        if let None = *self.native.get() {
            *(self.native.get()) = Some(native);
        }
    }

    fn unpark(&self) {
        unsafe {
            match *self.native.get() {
                None => panic!("Parker used before fibre was created"),
                Some(ref native) => {
                    if libfibre::cfibre_unpark(*native) < 0 {
                        panic!("Cannot unpark fibre");
                    }
                }
            }
        }
    }
}

pub(crate) struct FibreInner {
    name: Option<CString>,
    pub(crate) parker: Parker,
}

// This is fine for the same reason Parker is Sync
unsafe impl Send for FibreInner {}

#[derive(Clone)]
pub struct Fibre {
    pub(crate) inner: Arc<FibreInner>,
}

impl Fibre {
    /*
     * Internal API to create a new Fibre object
     * This should *only* be called from the fibre itself
     */
    pub(crate) fn new(name: Option<CString>) -> Fibre {
        Fibre {
            inner: Arc::new(FibreInner {
                name,
                parker: Parker::new(),
            }),
        }
    }

    /*
     * Panics if used before the Fibre starts running
     * This may happen for a brief period after spawn()
     */
    pub fn unpark(&self) {
        self.inner.parker.unpark();
    }

    pub fn name(&self) -> Option<&str> {
        self.cname()
            .map(|s| unsafe { std::str::from_utf8_unchecked(s.to_bytes()) })
    }

    pub fn cname(&self) -> Option<&CStr> {
        self.inner.name.as_deref()
    }
}
