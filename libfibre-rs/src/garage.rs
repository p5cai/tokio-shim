// Pooled fibres parked inside a "Garage"
// This amortizes large parts of the fibre-spawning overhead
// because of the light-weightedness of userspace scheduling,
// having many, many fibres sleeping would not in general be a problem
use crate::*;
use std::cell::UnsafeCell;
use std::sync::Arc;
use lazy_static::lazy_static;

lazy_static! {
    static ref GARAGE: sync::Mutex<Garage> = sync::Mutex::new(Garage::new());
}

pub fn spawn_garage<F>(f: F)
where
    F: 'static + Send + FibreSafe + FnOnce()
{
    if let Some(f) = GARAGE.lock().unwrap().run(Box::new(f)) {
        let main = move || {
            f();

            loop {
                let work = GARAGE.lock().unwrap().park();
                work();
            }
        };
        let main = unsafe { AssertFibreSafe::new_unchecked(main) };
        spawn(main);
    }
}

trait WorkFn: FibreSafe + FnOnce() {}

impl<T> WorkFn for T where T: FibreSafe + FnOnce() {}

struct Link {
    next: Option<Arc<Link>>,
    cond: sync::Condvar,
    run: UnsafeCell<Option<Box<dyn WorkFn + Send + 'static>>>
}

// This is okay *only* because of how Link is used in Garage
// specifically, the `run` field is only accessed by either park()
// or run(), but never at the same time. park() only does it after
// being woken up from the condition variable, while run() only
// does it before calling notify_one() on the condvar.
unsafe impl Sync for Link {}

struct Garage {
    stack: Option<Arc<Link>>
}

impl Garage {
    fn new() -> Garage {
        Garage {
            stack: None
        }
    }

    // Block the current fibre, until we have something to run
    // The caller shall acquire the mutex and pass it in
    fn park(mut self: sync::MutexGuard<Self>) -> Box<dyn WorkFn> {
        // Put ourself on to the linked list to indicate we want work
        let stack = std::mem::replace(&mut self.stack, None);
        let link = Arc::new(Link {
            next: stack,
            cond: sync::Condvar::new(),
            run: UnsafeCell::new(None)
        });
        self.stack = Some(link.clone());

        std::mem::drop(link.cond.wait(self).unwrap());

        // Now we have been notified, we have task to do
        return unsafe { std::mem::replace(&mut *link.run.get(), None) }.unwrap();
    }

    // Returns Some(task) if no fibre is available for the work (and the caller shall spawn a new fibre)
    // TODO: Can we do something like JoinHandle for these tasks?
    fn run(mut self: sync::MutexGuard<Self>, task: Box<dyn WorkFn + Send + 'static>) -> Option<Box<dyn WorkFn + Send + 'static>> {
        if self.stack.is_none() {
            return Some(task);
        }
        
        let link = self.stack.as_ref().unwrap().clone();
        self.stack = link.next.clone();
        std::mem::drop(self); // This is okay because cond is per-node
        unsafe {
            *link.run.get() = Some(task);
        }
        link.cond.notify_one();
        None
    }
}
