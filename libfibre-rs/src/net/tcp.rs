use libc as c;
use libfibre_sys as libfibre;
use std::io::{Error as IOError, Result, Read, Write};
use std::mem::size_of;
use std::net::{SocketAddr, ToSocketAddrs};
use std::os::raw::c_int;
use std::os::unix::io::{AsRawFd, FromRawFd, RawFd};
use std::time::Duration;

pub struct TcpListener {
    inner: c_int,
    addr: SocketAddr,
}

// TODO: implement all std APIs
impl TcpListener {
    /*
     * Try binding to one or more addresses one by one, and returns when
     * any one of them binded successfully. If all failed, the error value
     * is set to the last one we encountered.
     *
     * Mirrors TcpListener::bind in std.
     */
    pub fn bind<A: ToSocketAddrs>(addr: A) -> Result<TcpListener> {
        let mut last_err = None;
        for addr in addr.to_socket_addrs()? {
            match Self::_try_bind_once(addr) {
                Ok(listener) => return Ok(listener),
                Err(e) => last_err = Some(e),
            }
        }
        return Err(last_err.unwrap());
    }

    fn _try_bind_once(addr: SocketAddr) -> Result<TcpListener> {
        let (sa, len) = super::sockaddr_to_raw(&addr);
        let fd = unsafe { libfibre::cfibre_socket(sa.ss_family as c_int, libc::SOCK_STREAM, 0) };
        if fd < 0 {
            crate::_return_errno_as_err!();
        }

        let bind_res = unsafe { libfibre::cfibre_bind(fd, &sa as *const _ as *const _, len) };
        if bind_res < 0 {
            unsafe { libfibre::cfibre_close(fd) };
            crate::_return_errno_as_err!();
        }

        // backlog = 128 to match std::sys_common::net
        let listen_res = unsafe { libfibre::cfibre_listen(fd, 128) };
        if listen_res < 0 {
            unsafe { libfibre::cfibre_close(fd) };
            crate::_return_errno_as_err!();
        }

        // cfibre_bind could rewrite the addr due to automatic port allocation
        let addr = unsafe { super::sockaddr_from_raw(&sa as *const _ as *const _) };
        return Ok(TcpListener { inner: fd, addr });
    }

    pub fn local_addr(&self) -> Result<SocketAddr> {
        // This actually never fails; I don't know if it's due to difference between libfibre and POSIX APIs,
        // but we kind of have to store the address in the struct, not retrieving it as needed
        // TODO: investigate this.
        Ok(self.addr.clone())
    }

    fn _accept(&self) -> Result<TcpStream> {
        let mut addr = [0; std::mem::size_of::<c::sockaddr_storage>()];
        let mut addr_len = std::mem::size_of::<c::sockaddr_storage>() as c::socklen_t;
        let fd = unsafe {
            libfibre::cfibre_accept(self.inner, &mut addr as *mut _ as *mut _, &mut addr_len)
        };

        if fd < 0 {
            crate::_return_errno_as_err!();
        }

        let peer_addr = unsafe { super::sockaddr_from_raw(&addr as *const _ as *const _) };

        let stream = TcpStream {
            inner: fd,
            peer_addr,
            local_addr: self.addr.clone(),
        };

        Ok(stream)
    }

    /*
     * Blocks until a connection is accepted from the TCP socket
     * Returns the corresponding stream and the peer address
     */
    pub fn accept(&self) -> Result<(TcpStream, SocketAddr)> {
        let stream = self._accept()?;
        let addr = stream.peer_addr.clone();
        Ok((stream, addr))
    }

    /*
     * Returns an iterator which emits all incoming connections
     */
    pub fn incoming<'a>(&'a self) -> Incoming<'a> {
        Incoming { inner: self }
    }
}

impl AsRawFd for TcpListener {
    fn as_raw_fd(&self) -> RawFd {
        self.inner
    }
}

impl FromRawFd for TcpListener {
    unsafe fn from_raw_fd(fd: RawFd) -> TcpListener {
        let mut addr = [0; std::mem::size_of::<c::sockaddr_storage>()];
        let mut addr_len = std::mem::size_of::<c::sockaddr_storage>() as c::socklen_t;

        unsafe {
            c::getsockname(fd, &mut addr as *mut _ as *mut _, &mut addr_len as *mut _ as *mut _);
        }

        let local_addr = unsafe { super::sockaddr_from_raw(&addr as *const _ as *const _) };

        TcpListener {
            inner: fd,
            addr: local_addr
        }
    }
}

impl Drop for TcpListener {
    fn drop(&mut self) {
        unsafe { libfibre::cfibre_close(self.inner) };
    }
}

pub struct Incoming<'a> {
    inner: &'a TcpListener,
}

impl<'a> Iterator for Incoming<'a> {
    type Item = Result<TcpStream>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.inner._accept())
    }
}

pub struct TcpStream {
    inner: c_int,
    peer_addr: SocketAddr,
    local_addr: SocketAddr,
}

// TODO: implement all std APIs
impl TcpStream {
    pub fn connect<A: ToSocketAddrs>(addr: A) -> Result<TcpStream> {
        let mut last_err = None;
        for addr in addr.to_socket_addrs()? {
            match Self::_try_connect_once(addr) {
                Ok(stream) => return Ok(stream),
                Err(e) => last_err = Some(e),
            }
        }
        return Err(last_err.unwrap());
    }

    fn _try_connect_once(addr: SocketAddr) -> Result<TcpStream> {
        let (sa, len) = super::sockaddr_to_raw(&addr);
        let fd = unsafe { libfibre::cfibre_socket(sa.ss_family as c_int, libc::SOCK_STREAM, 0) };
        if fd < 0 {
            crate::_return_errno_as_err!();
        }

        let res = unsafe { libfibre::cfibre_connect(fd, &sa as *const _ as *const _, len) };

        if res < 0 {
            unsafe { libfibre::cfibre_close(fd) };
            crate::_return_errno_as_err!();
        }

        Ok(unsafe { TcpStream::from_raw_fd(fd) })
    }

    pub fn peer_addr(&self) -> Result<SocketAddr> {
        Ok(self.peer_addr.clone())
    }

    pub fn local_addr(&self) -> Result<SocketAddr> {
        Ok(self.local_addr.clone())
    }

    pub fn set_nodelay(&self, nodelay: bool) -> Result<bool> {
        let val: usize = if nodelay { 1 } else { 0 };
        let ret = unsafe {
            c::setsockopt(self.inner, c::SOL_TCP, c::TCP_NODELAY,
                &val as *const _ as *const _, size_of::<usize>() as u32)
        };

        if ret == 0 {
            return Ok(nodelay);
        } else {
            crate::_return_errno_as_err!();
        }
    }

    pub fn set_linger(&self, linger: bool) -> Result<bool> {
        let val: usize = if linger { 1 } else { 0 };
        let ret = unsafe {
            c::setsockopt(self.inner, c::SOL_TCP, c::SO_LINGER,
                &val as *const _ as *const _, size_of::<usize>() as u32)
        };

        if ret == 0 {
            return Ok(linger);
        } else {
            crate::_return_errno_as_err!();
        }
    }

    pub fn shutdown(&self, how: std::net::Shutdown) -> Result<()> {
        let ret = unsafe {
            // TODO: We should make a libfibre-native version of this
            libc::shutdown(self.inner, match how {
                std::net::Shutdown::Read => libc::SHUT_RD,
                std::net::Shutdown::Write => libc::SHUT_WR,
                std::net::Shutdown::Both => libc::SHUT_RDWR
            })
        };

        if ret == 0 {
            return Ok(());
        } else {
            crate::_return_errno_as_err!();
        }
    }

    pub fn set_write_timeout(&self, dur: Option<Duration>) -> Result<()> {
        let ret = unsafe {
            c::setsockopt(self.inner, c::SOL_SOCKET, c::SO_SNDTIMEO,
                                   &crate::duration_to_timespec(dur.unwrap_or(Duration::from_secs(0))) as *const _ as *const _,
                                   std::mem::size_of::<c::timespec>() as u32)
        };

        if ret == 0 {
            return Ok(());
        } else {
            crate::_return_errno_as_err!();
        }
    }

    pub fn set_read_timeout(&self, dur: Option<Duration>) -> Result<()> {
        let ret = unsafe {
            c::setsockopt(self.inner, c::SOL_SOCKET, c::SO_RCVTIMEO,
                                   &crate::duration_to_timespec(dur.unwrap_or(Duration::from_secs(0))) as *const _ as *const _,
                                   std::mem::size_of::<c::timespec>() as u32)
        };

        if ret == 0 {
            return Ok(());
        } else {
            crate::_return_errno_as_err!();
        }
    }

    pub fn try_clone(&self) -> Result<TcpStream> {
        let ret = unsafe {
            libfibre::cfibre_dup(self.inner)
        };

        if ret < 0 {
            crate::_return_errno_as_err!();
        } else {
            return Ok(unsafe { TcpStream::from_raw_fd(ret) });
        }
    }
}

impl Read for TcpStream {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let res = unsafe {
            libfibre::cfibre_read(self.inner, buf as *mut _ as *mut _, buf.len())
        };
        
        if res < 0 {
            crate::_return_errno_as_err!();
        }

        Ok(res as usize)
    }
}

impl Write for TcpStream {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let res = unsafe {
            libfibre::cfibre_write(self.inner, buf as *const _ as *const _, buf.len())
        };

        if res < 0 {
            crate::_return_errno_as_err!();
        }

        Ok(res as usize)
    }

    fn flush(&mut self) -> Result<()> {
        // You are not supposed to be able to flush a TCP socket
        Ok(())
    }
}

impl AsRawFd for TcpStream {
    fn as_raw_fd(&self) -> RawFd {
        self.inner
    }
}

impl FromRawFd for TcpStream {
    unsafe fn from_raw_fd(fd: RawFd) -> TcpStream {
        let mut addr = [0; std::mem::size_of::<c::sockaddr_storage>()];
        let mut addr_len = std::mem::size_of::<c::sockaddr_storage>() as c::socklen_t;

        unsafe {
            c::getpeername(fd, &mut addr as *mut _ as *mut _, &mut addr_len as *mut _ as *mut _);
        }

        let peer_addr = unsafe { super::sockaddr_from_raw(&addr as *const _ as *const _) };

        unsafe {
            c::getsockname(fd, &mut addr as *mut _ as *mut _, &mut addr_len as *mut _ as *mut _);
        }

        let local_addr = unsafe { super::sockaddr_from_raw(&addr as *const _ as *const _) };

        TcpStream {
            inner: fd,
            peer_addr,
            local_addr
        }
    }
}

impl Drop for TcpStream {
    fn drop(&mut self) {
        unsafe { libfibre::cfibre_close(self.inner) };
    }
}
