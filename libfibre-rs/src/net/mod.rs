mod tcp;

pub use tcp::{TcpListener, TcpStream, Incoming};

use std::net::Ipv6Addr;
pub use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4, SocketAddrV6, ToSocketAddrs};

use libc as c;

#[macro_export]
macro_rules! _return_errno_as_err {
    () => {
        return Err(IOError::from_raw_os_error(unsafe {
            libfibre::cfibre_get_errno()
        }));
    };
}

/*
 * Construct an *owned* sockaddr struct from a Rust SocketAddr
 * Note that this is NOT performance-optimized because SocketAddr
 * themselves are implemented internally as sockaddr structs
 * However, in order not to depened on the internal details of std,
 * we have to make a workaround like this.
 * Hopefully this won't be too much of an issue, as sockaddr should not be
 * constructed too often. If this becomes an issue in the future,
 * then we will have to clone our own SocketAddr implementation, or
 * use unsafe magic to access std internals.
 */
fn sockaddr_to_raw(addr: &SocketAddr) -> (c::sockaddr_storage, c::socklen_t) {
    let mut ret = [0u8; std::mem::size_of::<c::sockaddr_storage>()];
    let ret_len;

    match addr {
        SocketAddr::V4(ref a) => {
            let addr_in = c::sockaddr_in {
                sin_family: c::AF_INET as c::sa_family_t,
                sin_addr: unsafe { std::mem::transmute(a.ip().octets()) },
                sin_port: a.port().to_be(),
                sin_zero: [0; 8],
            };

            let byte_repr: [u8; std::mem::size_of::<c::sockaddr_in>()] =
                unsafe { std::mem::transmute(addr_in) };

            ret_len = std::mem::size_of::<c::sockaddr_in>();
            ret[0..ret_len].clone_from_slice(&byte_repr);
        }
        SocketAddr::V6(ref a) => {
            let addr_in = c::sockaddr_in6 {
                sin6_family: c::AF_INET6 as c::sa_family_t,
                sin6_addr: unsafe { std::mem::transmute(a.ip().octets()) },
                sin6_port: a.port().to_be(),
                sin6_flowinfo: a.flowinfo(),
                sin6_scope_id: a.scope_id(),
            };

            let byte_repr: [u8; std::mem::size_of::<c::sockaddr_in6>()] =
                unsafe { std::mem::transmute(addr_in) };

            ret_len = std::mem::size_of::<c::sockaddr_in6>();
            ret[0..ret_len].clone_from_slice(&byte_repr);
        }
    }

    (unsafe { std::mem::transmute(ret) }, ret_len as c::socklen_t)
}

/*
 * As with sockaddr_to_raw, this is probably not the most optimal implementation.
 * To be improved in the future, but that probably requires relying on the unstable
 * structure of std, or we need to implement our own SocketAddr.
 * This function assumes that the length of the sockaddr has been checked by the caller,
 * so that casting it to its corresponding sockaddr_in{,6} will not be unsound
 */
unsafe fn sockaddr_from_raw(addr: *const c::sockaddr) -> SocketAddr {
    unsafe {
        let fam = (*addr).sa_family;

        match fam as c::c_int {
            libc::AF_INET => {
                let addr: *const c::sockaddr_in = std::mem::transmute(addr);
                let ip_octets: [u8; 4] = std::mem::transmute((*addr).sin_addr.clone());

                SocketAddr::V4(SocketAddrV4::new(
                    Ipv4Addr::new(ip_octets[0], ip_octets[1], ip_octets[2], ip_octets[3]),
                    (*addr).sin_port,
                ))
            }
            libc::AF_INET6 => {
                let addr: *const c::sockaddr_in6 = std::mem::transmute(addr);
                let ip_seg: [u16; 8] = std::mem::transmute((*addr).sin6_addr.clone());
                let raddr = Ipv6Addr::new(
                    ip_seg[0], ip_seg[1], ip_seg[2], ip_seg[3], ip_seg[4], ip_seg[5], ip_seg[6],
                    ip_seg[7],
                );

                SocketAddr::V6(SocketAddrV6::new(
                    raddr,
                    (*addr).sin6_port,
                    (*addr).sin6_flowinfo,
                    (*addr).sin6_scope_id,
                ))
            }
            _ => {
                panic!("invalid address family")
            }
        }
    }
}
