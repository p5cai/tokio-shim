use super::MutexGuard;
use std::cell::UnsafeCell;
use std::mem::MaybeUninit;
use std::sync::LockResult;
use std::ops::DerefMut;
use libfibre_sys as libfibre;

pub struct Condvar {
    inner: UnsafeCell<Box<libfibre::cfibre_cond_t>>
}

impl Condvar {
    pub fn new() -> Condvar {
        let mut cond: Box<MaybeUninit<libfibre::cfibre_cond_t>> = Box::new(MaybeUninit::uninit());

        unsafe {
            if libfibre::cfibre_cond_init(cond.as_mut_ptr(), std::ptr::null_mut()) < 0 {
                panic!("Cannot initialize condvar");
            }
        }

        Condvar {
            inner: UnsafeCell::new(unsafe { std::mem::transmute(cond) })
        }
    }

    // This currently never fails because we have not implemented poisoning in our locks (Mutex)
    // TODO: Fix after implementing poisoning
    pub fn wait<'a, T>(&self, guard: MutexGuard<'a, T>) -> LockResult<MutexGuard<'a, T>> {
        unsafe {
            if libfibre::cfibre_cond_wait((*self.inner.get()).deref_mut() as *mut _, guard.raw_mutex as *mut _) < 0 {
                panic!("cfibre_cond_wait should not fail");
            }
        }

        // Change this when poisoning is implemented
        Ok(guard)
    }

    pub fn notify_one(&self) {
        unsafe {
            libfibre::cfibre_cond_signal((*self.inner.get()).deref_mut() as *mut _);
        }
    }

    pub fn notify_all(&self) {
        unsafe {
            libfibre::cfibre_cond_broadcast((*self.inner.get()).deref_mut() as *mut _);
        }
    }
}

impl Drop for Condvar {
    fn drop(&mut self) {
        unsafe {
            libfibre::cfibre_cond_destroy((*self.inner.get()).deref_mut() as *mut _);
        }
    }
}

unsafe impl Send for Condvar {}
unsafe impl Sync for Condvar {}
