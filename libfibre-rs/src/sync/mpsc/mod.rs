mod blocking;
mod mpsc_queue;
mod shared;

use std::sync::Arc;
use std::sync::mpsc::{SendError, TryRecvError, RecvError};

/// Asynchronous (infinite buffer) channel
pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let a = Arc::new(shared::Packet::<T>::new());
    a.inherit_blocker(None, a.postinit_lock());
    (Sender::new(a.clone()), Receiver::new(a.clone()))
}

// TODO: Implement flavor switching?
pub struct Sender<T> {
    inner: Arc<shared::Packet<T>>
}

unsafe impl<T: Send> Send for Sender<T> {}

impl<T> !Sync for Sender<T> {}

pub struct Receiver<T> {
    inner: Arc<shared::Packet<T>>
}

unsafe impl<T: Send> Send for Receiver<T> {}

impl<T> !Sync for Receiver<T> {}

impl<T> Sender<T> {
    fn new(inner: Arc<shared::Packet<T>>) -> Sender<T> {
        Sender { inner }
    }

    pub fn send(&self, t: T) -> Result<(), SendError<T>> {
        self.inner.send(t).map_err(SendError)
    } 
}

impl<T> Clone for Sender<T> {
    fn clone(&self) -> Self {
        self.inner.clone_chan();
        return Sender::new(self.inner.clone());
    }
}

impl<T> Drop for Sender<T> {
    fn drop(&mut self) {
        self.inner.drop_chan();
    }
}

impl<T> Receiver<T> {
    fn new(inner: Arc<shared::Packet<T>>) -> Receiver<T> {
        Receiver { inner }
    }

    pub fn try_recv(&self) -> Result<T, TryRecvError> {
        match self.inner.try_recv() {
            Ok(t) => Ok(t),
            Err(shared::Empty) => Err(TryRecvError::Empty),
            Err(shared::Disconnected) => Err(TryRecvError::Disconnected)
        }
    }

    pub fn recv(&self) -> Result<T, RecvError> {
        match self.inner.recv(None) {
            Ok(t) => Ok(t),
            Err(shared::Empty) => unreachable!(),
            Err(shared::Disconnected) => Err(RecvError)
        }
    }

    pub fn iter(&self) -> Iter<'_, T> {
        Iter { rx: self }
    }

    pub fn try_iter(&self) -> TryIter<'_, T> {
        TryIter { rx: self }
    }
}

impl<'a, T> IntoIterator for &'a Receiver<T> {
    type Item = T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<T> IntoIterator for Receiver<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter { rx: self }
    }
}

pub struct Iter<'a, T> {
    rx: &'a Receiver<T>
}

impl<T> Iterator for Iter<'_, T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.rx.recv().ok()
    }
}

pub struct TryIter<'a, T> {
    rx: &'a Receiver<T>
}

impl<T> Iterator for TryIter<'_, T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.rx.try_recv().ok()
    }
}

pub struct IntoIter<T> {
    rx: Receiver<T>
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.rx.recv().ok()
    }
}