use libfibre_sys as libfibre;
use std::cell::UnsafeCell;
use std::mem::MaybeUninit;
use std::ops::{Deref, DerefMut};
use std::sync::{LockResult, TryLockError, TryLockResult};

pub struct MutexGuard<'a, T> {
    data: &'a mut T,
    pub(super) raw_mutex: &'a mut libfibre::cfibre_mutex_t
}

impl<'a, T> Drop for MutexGuard<'a, T> {
    fn drop(&mut self) {
        unsafe {
            libfibre::cfibre_mutex_unlock(self.raw_mutex);
        }
    }
}

impl<'a, T> Deref for MutexGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.data
    }
}

impl<'a, T> DerefMut for MutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.data
    }
}

impl<'a, T> !Send for MutexGuard<'a, T> {}

// TODO: We do not currently implement poisoning due to the unclear semantics of panics in fibres
// TODO: Fix this
pub struct Mutex<T: ?Sized> {
    // TODO: We probably don't really need this box, but we'll make it a libfibre guarantee
    raw_mutex: UnsafeCell<Box<libfibre::cfibre_mutex_t>>,
    data: UnsafeCell<T>,
}

impl<T> Mutex<T> {
    pub fn new(data: T) -> Mutex<T> {
        let mut mutex_attrs: MaybeUninit<libfibre::cfibre_mutexattr_t> = MaybeUninit::uninit();
        let mut raw_mutex: Box<MaybeUninit<libfibre::cfibre_mutex_t>> = Box::new(MaybeUninit::uninit());

        unsafe {
            // TODO: We shouldn't really use panics ourselves here...
            if libfibre::cfibre_mutexattr_init(mutex_attrs.as_mut_ptr()) < 0 {
                panic!("Unexpected error while creating mutex");
            }

            if libfibre::cfibre_mutexattr_settype(mutex_attrs.as_mut_ptr(), libfibre::CFIBRE_MUTEX_DEFAULT) < 0 {
                panic!("Unexpected error while creating mutex");
            }

            if libfibre::cfibre_mutex_init(raw_mutex.as_mut_ptr(), mutex_attrs.as_ptr()) < 0 {
                panic!("Unexpected error while creating mutex");
            }

            if libfibre::cfibre_mutexattr_destroy(mutex_attrs.as_mut_ptr()) < 0 {
                panic!("Unexpected error while creating mutex");
            }
        }
        
        Mutex { 
            raw_mutex: UnsafeCell::new(unsafe { std::mem::transmute(raw_mutex) }),
            data: UnsafeCell::new(data)
        }
    }

    pub fn lock(&self) -> LockResult<MutexGuard<'_, T>> {
        unsafe {
            if libfibre::cfibre_mutex_lock((*self.raw_mutex.get()).deref_mut() as *mut _) < 0 {
                panic!("Unexpected error while locking mutex");
            }
        }
        
        Ok(MutexGuard {
            raw_mutex: unsafe { (*self.raw_mutex.get()).deref_mut() },
            data: unsafe { &mut *self.data.get() }
        })
    }

    pub fn try_lock(&self) -> TryLockResult<MutexGuard<'_, T>> {
        unsafe {
            if libfibre::cfibre_mutex_trylock((*self.raw_mutex.get()).deref_mut() as *mut _) < 0 {
                return Err(TryLockError::WouldBlock);
            }
        }

        Ok(MutexGuard {
            raw_mutex: unsafe { (*self.raw_mutex.get()).deref_mut() },
            data: unsafe { &mut *self.data.get() }
        })
    }
}

unsafe impl<T: ?Sized + Send> Send for Mutex<T> {}
unsafe impl<T: ?Sized + Send> Sync for Mutex<T> {}