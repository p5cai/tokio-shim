#![feature(fn_traits)]
#![feature(unboxed_closures)]
#![feature(negative_impls)]
#![feature(arbitrary_self_types)]

mod builder;
mod handle;
#[macro_use]
mod local;
pub mod net;
pub mod sync;
pub mod garage;

pub use builder::{Builder, FibreAttrs};
pub use handle::{Fibre, JoinHandle};
pub use local::{AccessError, LocalKey, LocalKeyInner};
pub use garage::spawn_garage;

use libfibre_sys as libfibre;

use std::any::Any;
use std::cell::RefCell;
use std::time::Duration;

// mirrors std::thread::Result
pub type Result<T> = std::result::Result<T, Box<dyn Any + Send + 'static>>;

/*
 * By implementing this marker trait for a function, you assert that the function
 * is safe to use in a fibre context. This means that the function MUST NOT access
 * any thread-local state, including any form of thread-local storage. In special
 * cases, this could be allowed as long as care is taken when accessing said storage.
 * 
 * Panicking from a fibre is currently NOT FibreSafe due to the internal usage of
 * thread-local storage for the panic counter. This would require standard library
 * modification to work.
 * 
 * Types with !Send and/or !Sync do not necessarily mean Fibre-unsafety. As long as
 * no thread-local state is accessed by / visible to the type, it is impossible to
 * distinguish whether a fibre has been scheduled onto another thread.
 */
pub unsafe trait FibreSafe {}

/*
 * Wrapper to assert that a type is safe to use in a fibre context.
 * By creating this wrapper around a type, you assert to uphold the same
 * security guarantees as laid out in the description of FibreSafe
 */
pub struct AssertFibreSafe<T> {
    inner: T,
}

impl<T> AssertFibreSafe<T> {
    /*
     * Creating this wrapper is unsafe due to the safety assertions
     */
    pub unsafe fn new_unchecked(inner: T) -> AssertFibreSafe<T> {
        AssertFibreSafe { inner }
    }
}

impl<T: FibreSafe> AssertFibreSafe<T> {
    /*
     * It is safe to wrap this around a type that is FibreSafe
     * to begin with
     */
    pub fn new(inner: T) -> AssertFibreSafe<T> {
        unsafe { Self::new_unchecked(inner) }
    }
}

/*
 * The programmer has asserted safety when creating the struct unsafely
 */
unsafe impl<T> FibreSafe for AssertFibreSafe<T> {}

impl<Args, R, F> FnOnce<Args> for AssertFibreSafe<F>
where
    F: FnOnce<Args, Output = R>,
{
    type Output = R;

    extern "rust-call" fn call_once(self, args: Args) -> R {
        self.inner.call_once(args)
    }
}

// Use a mutable static global to record the initialized state
// This is fine (safe) because it will only be changed from false to true
// once, and will only be read since then
pub(crate) static mut INITIALIZED: bool = false;

fibre_local! {
    pub(crate) static FIBRE_INFO: RefCell<Option<Fibre>> = RefCell::new(None);
}

fn _set_panic_handler() {
    std::panic::set_hook(Box::new(|_info| {
        match try_current() {
            Some(fibre) => {
                println!(
                    "panic happend in fibre <{}>",
                    fibre.name().unwrap_or("unnamed")
                );
                println!("{:?}", _info);
                // TODO: print stack trace if available?
            }
            // TODO: invoke default panic handler if panicked outside libfibre
            None => (),
        }
    }));
}

/*
 * Initialize the libfibre runtime
 * This or init_n() MUST be called before spawning any fibres
 */
pub fn init() {
    unsafe {
        if !INITIALIZED {
            _set_panic_handler();
            libfibre::cfibre_init();
            INITIALIZED = true;
        }
    }
}

/*
 * Initialize the libfibre runtime with specified poller and worker count
 * This or init() MUST be called before spawning any fibres
 */
pub fn init_n(poller_count: usize, worker_count: usize) {
    unsafe {
        if !INITIALIZED {
            _set_panic_handler();
            libfibre::cfibre_init_n(poller_count, worker_count);
            INITIALIZED = true;
        }
    }
}

/*
 * Spawn a new fibre with default parameters and acquire a handle to later
 * join the fibre for its result. For customized attributes, use the Builder interface
 *
 * This function is marked safe but is only usable with functions that are marked
 * FibreSafe. See the documentation of the FibreSafe trait for the safety guarantees
 * required.
 */
pub fn spawn<F, T>(f: F) -> JoinHandle<T>
where
    F: FibreSafe + FnOnce() -> T,
    F: Send + 'static,
    T: Send + 'static,
{
    Builder::new()
        .stack_size(2048 * 1024 * 1024) // match Rust's 2MB stack size
        .spawn(f)
        .expect("Failed to spawn the fibre")
}

/*
 * Get a handle to the Fibre object of self
 * Panics if not called from within a fibre created by spawn()
 */
pub fn current() -> Fibre {
    try_current().expect("current() called outside of a spawned fibre")
}

/*
 * A version of current() that does not panic but returns None
 * if not called within a fibre created by spawn()
 */
pub fn try_current() -> Option<Fibre> {
    FIBRE_INFO.with(|info| info.borrow().clone())
}

pub(crate) fn duration_to_timespec(dur: Duration) -> libfibre::timespec {
    libfibre::timespec {
        tv_sec: dur.as_secs() as i64,
        tv_nsec: dur.subsec_micros() as i64
    }
}

/*
 * Put the current fibre to sleep for some duration
 */
pub fn sleep(dur: Duration) {
    if unsafe { !INITIALIZED } {
        return;
    }

    let mut req = duration_to_timespec(dur);

    let mut rem = libfibre::timespec {
        tv_sec: 0,
        tv_nsec: 0
    };

    unsafe {
        while libfibre::cfibre_nanosleep(&req as *const _, &mut rem as *mut _) != 0 {
           req = rem; 
        }
    }
}

/*
 * Put the current fibre to sleep until it is unparked by another fibre
 * The fibre can be unparked using the Fibre type acquired from current()
 */
pub fn park() {
    if unsafe { !INITIALIZED } {
        return;
    }

    unsafe {
        libfibre::cfibre_park();
    }
}

/*
 * Yield execution from the current fibre
 */
pub fn yield_now() {
    if unsafe { !INITIALIZED } {
        return;
    }
    
    unsafe {
        libfibre::cfibre_yield();
    }
}