// TODO: Can we also implement something like async-shim to recursively check for fibre-safety?

extern crate proc_macro;

use proc_macro::TokenStream as TokenStream1;
use proc_macro2::*;
use syn::*;
use syn::punctuated::Punctuated;
use syn::token::*;
use quote::*;

fn rewrite_block(block: &mut Block) {
    for stmt in block.stmts.iter_mut() {
        match stmt {
            Stmt::Local(ref mut local) => {
                if let Some((_, ref mut expr)) = local.init {
                    rewrite_expr(expr);
                }
            },
            Stmt::Item(_) => {
                // Nothing
            },
            Stmt::Expr(ref mut expr) => {
                rewrite_expr(expr);
            },
            Stmt::Semi(ref mut expr, _) => {
                rewrite_expr(expr);
            }
        }
    }
}

fn rewrite_punctuated<P>(punc: &mut Punctuated<Expr, P>) {
    for expr in punc.iter_mut() {
        rewrite_expr(expr);
    }
}

fn rewrite_expr(expr: &mut Expr) {
    let mut new_expr: Option<Expr> = None;

    match expr {
        Expr::Await(ref mut expr_await) => {
            rewrite_expr(&mut expr_await.base);

            // All .await expands to the _await method call (see FutureShim)
            new_expr = Some(Expr::MethodCall(ExprMethodCall {
                attrs: expr_await.attrs.clone(),
                receiver: expr_await.base.clone(),
                dot_token: Token![.](expr_await.await_token.span),
                method: Ident::new("_await", expr_await.await_token.span),
                turbofish: None,
                paren_token: Paren { span: expr_await.await_token.span },
                args: Punctuated::new()
            }));
        }
        Expr::Array(ref mut expr_array) => {
            rewrite_punctuated(&mut expr_array.elems);
        },
        Expr::Assign(ref mut expr_assign) => {
            rewrite_expr(&mut expr_assign.left);
            rewrite_expr(&mut expr_assign.right);
        },
        Expr::AssignOp(ref mut expr_assign_op) => {
            rewrite_expr(&mut expr_assign_op.left);
            rewrite_expr(&mut expr_assign_op.right);
        },
        Expr::Async(ref mut expr_async) => {
            rewrite_block(&mut expr_async.block);

            // All async {} blocks get replaced with a closure
            // The closure would also implement FutureShim (thanks to default impl for Fn traits)
            let closure = Expr::Closure(ExprClosure {
                attrs: expr_async.attrs.clone(),
                movability: None,
                asyncness: None,
                capture: expr_async.capture,
                or1_token: Token![|](expr_async.async_token.span),
                inputs: Punctuated::new(),
                or2_token: Token![|](expr_async.async_token.span),
                output: ReturnType::Default,
                body: std::boxed::Box::new(Expr::Block(ExprBlock {
                    attrs: expr_async.attrs.clone(),
                    label: None,
                    block: expr_async.block.clone()
                }))
            });
            new_expr = Some(parse_quote! {
                futures_shim::FutureShimCompat::from_closure(#closure)
            });
        },
        Expr::Binary(ref mut expr_binary) => {
            rewrite_expr(&mut expr_binary.left);
            rewrite_expr(&mut expr_binary.right);
        },
        Expr::Block(ref mut expr_block) => {
            rewrite_block(&mut expr_block.block)
        },
        Expr::Box(_) => {},
        Expr::Break(_) => {},
        Expr::Call(ref mut expr_call) => {
            rewrite_expr(&mut expr_call.func);
            rewrite_punctuated(&mut expr_call.args);
        },
        Expr::Cast(_) => {},
        Expr::Closure(ref mut expr_closure) => {
            expr_closure.asyncness = None;
            rewrite_expr(&mut expr_closure.body);
        },
        Expr::Continue(_) => {},
        Expr::Field(_) => {},
        Expr::ForLoop(ref mut expr_for) => {
            rewrite_expr(&mut expr_for.expr);
            rewrite_block(&mut expr_for.body);
        },
        Expr::Group(_) => {},
        Expr::If(ref mut expr_if) => {
            rewrite_expr(&mut expr_if.cond);
            rewrite_block(&mut expr_if.then_branch);
            if let Some((_, ref mut else_expr)) = expr_if.else_branch {
                rewrite_expr(else_expr);
            }
        },
        Expr::Index(_) => {},
        Expr::Let(ref mut expr_let) => {
            rewrite_expr(&mut expr_let.expr);
        },
        Expr::Lit(_) => {},
        Expr::Loop(ref mut expr_loop) => {
            rewrite_block(&mut expr_loop.body)
        },
        Expr::Macro(_) => {},
        Expr::Match(ref mut expr_match) => {
            rewrite_expr(&mut expr_match.expr);
            for arm in expr_match.arms.iter_mut() {
                rewrite_expr(&mut arm.body);
            }
        },
        Expr::MethodCall(ref mut expr_method_call) => {
            rewrite_expr(&mut expr_method_call.receiver);
            rewrite_punctuated(&mut expr_method_call.args);
        },
        Expr::Paren(_) => {},
        Expr::Path(_) => {},
        Expr::Range(_) => {},
        Expr::Reference(_) => {},
        Expr::Repeat(_) => {},
        Expr::Return(_) => {},
        Expr::Struct(_) => {},
        Expr::Try(ref mut expr_try) => {
            rewrite_expr(&mut expr_try.expr);
        },
        Expr::TryBlock(_) => {},
        Expr::Tuple(_) => {},
        Expr::Type(_) => {},
        Expr::Unary(_) => {},
        Expr::Unsafe(_) => {},
        Expr::Verbatim(_) => {},
        Expr::While(ref mut expr_while) => {
            rewrite_expr(&mut expr_while.cond);
            rewrite_block(&mut expr_while.body);
        },
        Expr::Yield(_) => {},
        _ => {},
    }

    if let Some(new_expr) = new_expr {
        *expr = new_expr;
    }
}

#[proc_macro_attribute]
pub fn async_shim_main(_attr: TokenStream1, item: TokenStream1) -> TokenStream1 {
    let mut ast = parse_macro_input!(item as ItemFn);
    // Remove the async marker of all functions
    ast.sig.asyncness = None;
    rewrite_block(&mut ast.block);
    // TODO: Can we remove this and make it more similar to stock Tokio?
    ast.block.stmts.insert(0, parse_quote! {
        tokio_shim::init();
    });
    // This version of the macro does not apply the return type transformation
    // so that it works with the main function.
    return TokenStream1::from(ast.to_token_stream());
}

#[proc_macro_attribute]
pub fn async_shim(_attr: TokenStream1, item: TokenStream1) -> TokenStream1 {
    let mut ast = parse_macro_input!(item as ItemFn);
    // Remove the async marker of all functions
    ast.sig.asyncness = None;
    // Add the FutureShim trait wrapper for the return type
    match ast.sig.output.clone() {
        ReturnType::Type(arrow, t) => {
            ast.sig.output = ReturnType::Type(arrow, std::boxed::Box::new(parse_quote! {
                impl futures_shim::FakeFuture<Output = #t>
            }));
        },
        ReturnType::Default => {}
    };
    // Rewrite the function body to remove async / await
    rewrite_block(&mut ast.block);
    let block_expr = Expr::Block(ExprBlock {
        attrs: vec![],
        label: None,
        block: *ast.block.clone()
    });
    // Wrap the return value inside a closure to mimic Future
    // The closure implements FutureShim
    ast.block.stmts = vec![parse_quote! {
        return futures_shim::FutureShimCompat::from_closure(move || { #block_expr });
    }];
    return TokenStream1::from(ast.to_token_stream());
}