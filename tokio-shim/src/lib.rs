pub use futures_shim::*;

pub mod net;

pub fn spawn<T: 'static + Send + FutureShim<Output = ()>>(f: T) {
    // TODO: Can we do this better? This is whildly unsafe
    // (or maybe not, because Tokio programs cannot use thread locals in unsafe ways anyway?)
    libfibre_rs::spawn(unsafe { libfibre_rs::AssertFibreSafe::new_unchecked(move || {
        f._await();
    }) });
}

// TODO: Can we make this similar to how Tokio executors are normally initialized?
pub fn init() {
    libfibre_rs::init()
}