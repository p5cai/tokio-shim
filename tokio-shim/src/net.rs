use crate::{FakeFuture, FutureShimCompat};
use crate::io::{AsyncReadShim, AsyncWriteShim};
use std::io::{Read, Write, Result};
use libfibre_rs::net as snet;

pub struct TcpListener {
    inner: snet::TcpListener
}

impl TcpListener {
    pub fn bind<A: snet::ToSocketAddrs + Unpin>(addr: A) -> impl FakeFuture<Output = Result<TcpListener>> {
        FutureShimCompat::from_closure(move || {
            Ok(TcpListener {
                inner: snet::TcpListener::bind(addr)?
            })
        })
    }

    pub fn accept<'a>(&'a self) -> impl 'a + FakeFuture<Output = Result<(TcpStream, snet::SocketAddr)>> {
        FutureShimCompat::from_closure(move || {
            let (stream, addr) = self.inner.accept()?;
            Ok((TcpStream { inner: stream }, addr))
        })
    }
}

pub struct TcpStream {
    inner: snet::TcpStream
}

impl Read for TcpStream {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        self.inner.read(buf)
    }
}

impl Write for TcpStream {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        self.inner.write(buf)
    }

    fn flush(&mut self) -> Result<()> {
        self.inner.flush()
    }
}

impl AsyncReadShim for TcpStream {
    type Inner = snet::TcpStream;

    fn get_inner(&self) -> &Self::Inner {
        &self.inner
    }
    
    fn get_inner_mut(&mut self) -> &mut Self::Inner {
        &mut self.inner
    }

    fn into_inner(self) -> Self::Inner {
        self.inner
    }
}

impl AsyncWriteShim for TcpStream {
    type Inner = snet::TcpStream;

    fn get_inner(&self) -> &Self::Inner {
        &self.inner
    }

    fn get_inner_mut(&mut self) -> &mut Self::Inner {
        &mut self.inner
    }

    fn into_inner(self) -> Self::Inner {
        self.inner
    }
}