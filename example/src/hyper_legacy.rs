use std::io::copy;

use hyper::{Get, Post};
use hyper::server::{Server, Request, Response};
use hyper::uri::RequestUri::AbsolutePath;

macro_rules! try_return(
    ($e:expr) => {{
        match $e {
            Ok(v) => v,
            Err(e) => { println!("Error: {}", e); return; }
        }
    }}
);

fn echo(mut req: Request, mut res: Response) {
    match req.uri {
        AbsolutePath(ref path) => match (&req.method, &path[..]) {
            (&Get, "/plaintext") => {
                res.headers_mut().set_raw("Content-Type", vec![b"text/plain".to_vec()]);
                *res.status_mut() = hyper::Ok;
                try_return!(res.send(b"Hello, World!"));
            },
            _ => {
                *res.status_mut() = hyper::NotFound;
                return;
            }
        },
        _ => {
            return;
        }
    };
}

fn main() {
    libfibre_rs::init();
    let server = Server::http("0.0.0.0:1337").unwrap();
    let _guard = server.handle(echo);
    println!("Listening on http://0.0.0.0:1337");
}
