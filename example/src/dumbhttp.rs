// The simplest HTTP server possible, used for troubleshooting only
use libfibre_rs::net;
use std::io::{Read, Write};

fn main() -> std::io::Result<()> {
    libfibre_rs::init();

    let listener = net::TcpListener::bind("0.0.0.0:8080")?;

    while let Ok((conn, _addr)) = listener.accept() {
        let handler = move || {
            match handle_conn(conn) {
                Ok(()) => (),
                Err(e) => {
                    println!("{:?}", e);
                }
            }
        };
        let handler = unsafe {
            libfibre_rs::AssertFibreSafe::new_unchecked(handler)
        };
        libfibre_rs::spawn(handler);
    }

    Ok(())
}

fn handle_conn(mut conn: net::TcpStream) -> std::io::Result<()> {
    let mut buf = [0u8; 512];

    loop {
        let len = conn.read(&mut buf)?;

        if buf[0..len].windows(2).position(|x| x == b"\r\n").is_some() {
            write!(&mut conn, "HTTP/1.1 200 OK\r\nContent-Length: 12\r\n\r\nHello World\n")?;
        }
    }
}