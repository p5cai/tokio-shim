use std::any::{Any, TypeId};

pub trait Typeable: Any {
    #[inline(always)]
    fn get_type(&self) -> TypeId { TypeId::of::<Self>() }
}

impl<T: Any> Typeable for T {}