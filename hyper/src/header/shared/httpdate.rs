use std::str::FromStr;
use std::fmt::{self, Display};

use time::OffsetDateTime;

const RFC2822_FORMAT_NO_OFFSET: &'static [time::format_description::FormatItem<'static>] =
    time::macros::format_description!("[weekday repr:short], [day] [month repr:short] [year] [hour]:[minute]:[second]");

const ASCTIME_FORMAT: &'static [time::format_description::FormatItem<'static>] =
    time::macros::format_description!("[weekday repr:short] [month repr:short] [day padding:space] [hour]:[minute]:[second] [year]");

/// A `time::Time` with HTTP formatting and parsing
///
//   Prior to 1995, there were three different formats commonly used by
//   servers to communicate timestamps.  For compatibility with old
//   implementations, all three are defined here.  The preferred format is
//   a fixed-length and single-zone subset of the date and time
//   specification used by the Internet Message Format [RFC5322].
//
//     HTTP-date    = IMF-fixdate / obs-date
//
//   An example of the preferred format is
//
//     Sun, 06 Nov 1994 08:49:37 GMT    ; IMF-fixdate
//
//   Examples of the two obsolete formats are
//
//     Sunday, 06-Nov-94 08:49:37 GMT   ; obsolete RFC 850 format
//     Sun Nov  6 08:49:37 1994         ; ANSI C's asctime() format
//
//   A recipient that parses a timestamp value in an HTTP header field
//   MUST accept all three HTTP-date formats.  When a sender generates a
//   header field that contains one or more timestamps defined as
//   HTTP-date, the sender MUST generate those timestamps in the
//   IMF-fixdate format.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct HttpDate(pub time::OffsetDateTime);

impl FromStr for HttpDate {
    type Err = crate::Error;
    fn from_str(s: &str) -> crate::Result<HttpDate> {
        // TODO: the default RFC2822 parser may not support all the time zone literals.
        if let Ok(date) = time::OffsetDateTime::parse(s, &time::format_description::well_known::Rfc2822) {
            return Ok(HttpDate(date));
        }

        if let Ok(date) = time::PrimitiveDateTime::parse(s, ASCTIME_FORMAT) {
            return Ok(HttpDate(OffsetDateTime::UNIX_EPOCH.clone().replace_date_time(date)));
        }
        
        // TODO: RFC850 support is removed for now

        return Err(crate::Error::Header);
    }
}

impl Display for HttpDate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let str = self.0.to_offset(time::macros::offset!(UTC)).format(RFC2822_FORMAT_NO_OFFSET)
            .map_err(|_| fmt::Error)?;
        fmt::Display::fmt(&format!("{} GMT", str), f)
    }
}

#[cfg(test)]
mod tests {
    use time::macros::datetime;
    use super::HttpDate;

    const NOV_07: HttpDate = HttpDate(datetime!(1994-11-07 08:48:37 UTC));

    #[test]
    fn test_imf_fixdate() {
        assert_eq!("Sun, 07 Nov 1994 08:48:37 GMT".parse::<HttpDate>().unwrap(), NOV_07);
    }

    #[test]
    fn test_asctime() {
        assert_eq!("Sun Nov  7 08:48:37 1994".parse::<HttpDate>().unwrap(), NOV_07);
    }

    #[test]
    fn test_no_date() {
        assert!("this-is-no-date".parse::<HttpDate>().is_err());
    }
}
