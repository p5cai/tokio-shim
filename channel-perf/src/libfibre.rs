// This benchmark is not fair to libfibre since it does no I/O and does not give
// much scheduling opportunities to the fibre scheduler.
// TODO: Can we derive a better test for channels than this?
mod common;

use libfibre_rs::{sync::mpsc::{channel, Sender}, AssertFibreSafe};

use tic::{Clocksource, Sample};

use self::common::*;

fn main() {
    libfibre_rs::init();
    let params = parse_args_or_exit("sync");
    let mut tic_receiver = create_tic_receiver(&params);

    let fr_sender = start_recv_thread(tic_receiver.get_clocksource(), tic_receiver.get_sender());

    for _ in 0..params.sender_threads {
        start_sender_thread(tic_receiver.get_clocksource(), tic_receiver.get_sender(), fr_sender.clone());
    }

    let main_task = move || {
        tic_receiver.run();
        print_metrics(&params, tic_receiver.clone_meters());
    };
    let main_task = unsafe { AssertFibreSafe::new_unchecked(main_task) };
    // Note: libfibre requires the main thread to be available for the runtime.
    // TODO: Fix this in libfibre_rs.
    libfibre_rs::spawn(main_task).join().unwrap();
}

fn start_sender_thread(clock: Clocksource, mut tic_sender: tic::Sender<Metric>, sender: Sender<Timing>) {
    let task = move || {
        let mut last_iteration = clock.counter();
        loop {
            let now = clock.counter();
            let timing = Timing {
                start: now,
            };
            sender.send(timing).unwrap();
            tic_sender.send(Sample::new(last_iteration, now, Metric::Send)).unwrap();
            last_iteration = now;
        }
    };
    let task = unsafe { AssertFibreSafe::new_unchecked(task) };
    libfibre_rs::spawn(task);
}

fn start_recv_thread(clock: Clocksource, mut sender: tic::Sender<Metric>) -> Sender<Timing> {
    let (tx, rx) = channel::<Timing>();
    let task = move || {
        let mut last_iteration_time = clock.counter();
        loop {
            let timing = rx.recv().expect("failed to receive message");
            let now = clock.counter();
            sender.send(Sample::new(timing.start, now, Metric::RecvLatency)).unwrap();
            sender.send(Sample::new(last_iteration_time, now, Metric::RecvLoopTime)).unwrap();
            last_iteration_time = now;
        }
    };
    let task = unsafe { AssertFibreSafe::new_unchecked(task) };
    libfibre_rs::spawn(task);
    tx
}